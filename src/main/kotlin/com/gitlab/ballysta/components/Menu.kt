//@file:Suppress("UNCHECKED_CAST")
//package com.gitlab.ballysta.components
//
//import com.gitlab.ballysta.architecture.*
//import com.gitlab.ballysta.components.bukkit.listen
//import org.bukkit.Bukkit
//import org.bukkit.Material
//import org.bukkit.Material.*
//import org.bukkit.entity.Player
//import org.bukkit.event.inventory.*
//import org.bukkit.inventory.InventoryView
//import org.bukkit.inventory.ItemStack
//import org.bukkit.inventory.meta.ItemMeta
//import org.bukkit.material.MaterialData
//
//typealias Displayable = Player.() -> (Unit)
//fun Player.display(displayable: Displayable) = displayable()
//
//interface Item {
//    var type: Material
//    var amount: Int
//
//    fun <Return> Item.meta(block: ItemMeta.() -> (Return)): Return
//    fun <Return> Item.data(block: MaterialData.() -> (Return)): Return
//
//    val onClicked: Event<() -> (Boolean)>
//}
//
//fun <Type> Item.meta(block: Type.() -> (Unit)) {
//    meta(block as ItemMeta.() -> (Unit))
//}
//fun <Type> Item.data(block: Type.() -> (Unit)) {
//    data(block as MaterialData.() -> (Unit))
//}
//
//var Item.name: String
//    get() = meta { displayName ?: "" }
//    set(value) = meta { setDisplayName("§r$value") }
//
//
//interface Slots {
//    operator fun set(index: Int, block: Item.() -> (Unit))
//}
//
//fun Toggled.Menu(size: Int, name: Any, block: Togglable.(Player, Slots) -> (Unit)): Displayable = {
//    val owner = this
//    openInventory(object : InventoryView(), Slots {
//        val component = Component {}
//        val items = Bukkit.createInventory(null, size, name.toString())
//
//        override fun set(index: Int, block: Item.() -> (Unit)) =
//            setItem(index, (getItem(index) ?: ItemStack(AIR, 1)).also {
//                block(object : Item {
//                    override var type: Material
//                        get() = it.type
//                        set(value) { it.type = value }
//                    override var amount: Int
//                        get() = it.amount
//                        set(value) { it.amount = value }
//
//                    override fun <Return> Item.meta(block: ItemMeta.() -> (Return)): Return {
//                        val meta = it.itemMeta
//                        val result = block(meta!!)
//                        it.itemMeta = meta
//                        return result
//                    }
//                    override fun <Return> Item.data(block: MaterialData.() -> (Return)): Return {
//                        val data = it.data
//                        val result = block(data!!)
//                        it.data = data
//                        return result
//                    }
//
//                    override val onClicked: Event<() -> (Boolean)> = {
//                        listen<InventoryClickEvent> {
//                            if (rawSlot == index && owner == player && !isCancelled)
//                                isCancelled = it()
//                        }
//                    }
//                })
//            })
//
//        override fun getPlayer() = owner
//        override fun getType() = items.type
//        override fun getBottomInventory() = owner.inventory
//        override fun getTitle() = name.toString()
//
//        override fun getTopInventory() = items
//
//        init {
//            block(component, owner, this)
//            listen<InventoryOpenEvent> { if (inventory == items) component.enable() }
//            listen<InventoryCloseEvent> { if (inventory == items) component.disable() }
//            onDisabled { inventory.viewers.forEach { it.closeInventory() } }
//        }
//    })
//}