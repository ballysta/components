package com.gitlab.ballysta.components.bukkit.player

import com.gitlab.ballysta.architecture.*
import com.gitlab.ballysta.components.handle
import io.netty.buffer.ByteBuf
import io.netty.handler.codec.EncoderException
import net.minecraft.server.v1_16_R3.ChatComponentText
import net.minecraft.server.v1_16_R3.IChatBaseComponent
import org.bukkit.ChatColor
import org.bukkit.Location
import org.bukkit.entity.ArmorStand
import org.bukkit.entity.Player
import java.nio.charset.StandardCharsets
import java.util.*

var idCounter = 0;

val Player.displayHologram: Toggled.(location: Location, lines: Table<Int, String>) -> (Unit)
    get() = { location: Location, lines: Table<Int, String> ->
        val indexIdMap = HashMap<Int, Int>()
        fun sendUpdateEntity(index: Int, text: String) {

            indexIdMap[index]?.let {
                handle(0x44) {
                    writeVarInt(it)

                    writeByte(0)
                    writeVarInt(0)
                    writeByte(0x20)

                    writeByte(2)
                    writeVarInt(5)
                    writeBoolean(true)
                    writeChatMessage(ChatComponentText(ChatColor.translateAlternateColorCodes('&', text)))

                    writeByte(3)
                    writeVarInt(7)
                    writeBoolean(true)

                    writeByte(4)
                    writeVarInt(7)
                    writeBoolean(true)

                    writeByte(5)
                    writeVarInt(7)
                    writeBoolean(true)

                    writeByte(14)
                    writeVarInt(0)
                    writeByte(0x10)

                    writeByte(255)
                }
            }
        }

        fun sendSpawnEntity(index: Int, text: String) {
            idCounter--;
            indexIdMap[index] = idCounter
            handle(0x02) {
                writeVarInt(idCounter)
                writeUUID(UUID.randomUUID())
                writeVarInt(1)
                writeDouble(location.x)
                writeDouble(location.y - (index * 0.25))
                writeDouble(location.z)
                writeByte((0 * 256.0f / 360.0f).toInt())
                writeByte((0 * 256.0f / 360.0f).toInt())
                writeByte((0 * 256.0f / 360.0f).toInt())
                writeShort(0)
                writeShort(0)
                writeShort(0)
            }
            sendUpdateEntity(index, text)

        }

        fun sendRemoveEntity(index: Int) {
            val id = indexIdMap[index]
            id?.let {
                indexIdMap.remove(index)
                handle(0x36) {
                    writeVarInt(1)
                    writeVarInt(id)
                }

            }
        }

        lines.onChanged { index, oldLine, newLine ->
            if (oldLine != null && newLine != null) {
                sendUpdateEntity(index, newLine)
            } else if (newLine == null) {
                sendRemoveEntity(index)
            } else {
                sendSpawnEntity(index, newLine)
            }
        }


        onEnabled {
            lines.forEach { index, line ->
                sendSpawnEntity(
                    index, line
                )

            }
        }


        onDisabled {
            lines.forEach { index, _ ->
                sendRemoveEntity(index)
            }
        }
    }

fun ByteBuf.writeChatMessage(message: IChatBaseComponent): ByteBuf {
    return this.writeString(IChatBaseComponent.ChatSerializer.a(message), 262144)
}

fun ByteBuf.writeVarInt(i: Int): ByteBuf {
    var variable = i;
    while (variable and -128 != 0) {
        writeByte(variable and 127 or 128)
        variable = variable ushr 7
    }

    writeByte(variable)
    return this
}

fun ByteBuf.writeUUID(uuid: UUID): ByteBuf {
    this.writeLong(uuid.mostSignificantBits)
    this.writeLong(uuid.leastSignificantBits)
    return this
}

fun ByteBuf.writeString(string: String, i: Int): ByteBuf {
    val abyte: ByteArray = string.toByteArray(StandardCharsets.UTF_8)
    return if (abyte.size > i) {
        throw EncoderException("String too big (was " + abyte.size + " bytes encoded, max " + i + ")")
    } else {
        this.writeVarInt(abyte.size)
        this.writeBytes(abyte)
        this
    }

}
