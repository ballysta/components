package com.gitlab.ballysta.components.bukkit

import org.bukkit.Bukkit
import org.bukkit.Bukkit.getPluginManager
import org.bukkit.command.Command
import org.bukkit.command.CommandSender
import org.bukkit.configuration.file.YamlConfiguration
import org.bukkit.event.Listener
import org.bukkit.generator.ChunkGenerator
import org.bukkit.plugin.Plugin
import org.bukkit.plugin.PluginDescriptionFile
import org.bukkit.plugin.PluginLoader
import java.io.File
import java.lang.ClassLoader.getSystemResourceAsStream
import java.nio.file.Files.copy
import java.nio.file.Files.exists
import java.util.jar.JarFile
import java.util.regex.Pattern.compile

class StubPlugin(private val name: String) : Plugin {
    private var naggable = false
    private var enabled = true
    private var description = PluginDescriptionFile(getName(), "1.0.0", "")
    private val config = YamlConfiguration()

    override fun getServer() = Bukkit.getServer()!!
    override fun getLogger() = Bukkit.getLogger()!!
    override fun getName() = name
    override fun getDescription() = description
    override fun getDataFolder() = File("plugins", name)
    override fun isEnabled() = enabled

    override fun isNaggable() = naggable
    override fun setNaggable(naggable: Boolean) {
        this.naggable = naggable
    }

    override fun getResource(uri: String) = getSystemResourceAsStream(uri)!!
    override fun saveResource(uri: String, replace: Boolean) {
        val file = dataFolder.toPath().resolve(uri)
        if (replace || !exists(file)) getResource(uri).use { copy(it, file) }
    }

    override fun getConfig() = config
    override fun reloadConfig() = config.load(File(dataFolder, "config.yml"))
    override fun saveConfig() = config.save(File(dataFolder, "config.yml"))
    override fun saveDefaultConfig() = saveResource("config.yml", false)

    override fun onDisable() {}
    override fun getDefaultWorldGenerator(p0: String, p1: String?): ChunkGenerator? {
        TODO("Not yet implemented")
    }

    override fun onLoad() {}
    override fun onEnable() {}

    override fun onTabComplete(
        sender: CommandSender, command: Command,
        alias: String, arguments: Array<String>
    ) = emptyList<String>()

    override fun onCommand(
        sender: CommandSender, command: Command,
        alias: String, arguments: Array<String>
    ) = false

    override fun getPluginLoader() = object : PluginLoader {
        override fun enablePlugin(plugin: Plugin) = getPluginManager().enablePlugin(plugin)
        override fun disablePlugin(plugin: Plugin) = getPluginManager().disablePlugin(plugin)

        override fun getPluginDescription(file: File) = JarFile(file).run {
            PluginDescriptionFile(getInputStream(getJarEntry("plugin.yml")))
        }

        override fun getPluginFileFilters() = arrayOf(compile("\\.jar$"))
        override fun loadPlugin(p0: File): Plugin {
            TODO("Not yet implemented")
        }

        override fun createRegisteredListeners(
            listener: Listener, plugin: Plugin
        ) = throw UnsupportedOperationException("Will implement later.")
    }
}