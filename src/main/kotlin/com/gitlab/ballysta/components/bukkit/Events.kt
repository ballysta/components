@file:Suppress("UNCHECKED_CAST", "NOTHING_TO_INLINE", "MISPLACED_TYPE_PARAMETER_CONSTRAINTS")
package com.gitlab.ballysta.components.bukkit

import com.gitlab.ballysta.architecture.Event
import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.TreeEvent
import org.bukkit.Bukkit.*
import org.bukkit.advancement.Advancement
import org.bukkit.entity.Entity
import org.bukkit.entity.Player
import org.bukkit.event.Cancellable
import org.bukkit.event.EventPriority.NORMAL
import org.bukkit.event.HandlerList
import org.bukkit.event.Listener
import org.bukkit.event.block.Action
import org.bukkit.event.block.BlockBreakEvent
import org.bukkit.event.block.BlockEvent
import org.bukkit.event.entity.*
import org.bukkit.event.player.PlayerAdvancementDoneEvent
import org.bukkit.event.player.PlayerEvent
import org.bukkit.event.player.PlayerInteractEvent
import org.bukkit.plugin.EventExecutor
import org.bukkit.plugin.RegisteredListener
import java.lang.reflect.Method
import java.util.*
import kotlin.reflect.KClass
import org.bukkit.event.Event as BukkitEvent

private val STUB_PLUGIN = StubPlugin("SuzumebachiEvents")
private val LISTENER_LISTS = IdentityHashMap<KClass<out BukkitEvent>, ListenerList>()

private class ListenerList(val type: KClass<out BukkitEvent>) : TreeEvent<(Any) -> (Unit)>(), Listener, EventExecutor {
    fun findHandler(type: Class<*>): Method? {
        if (type == BukkitEvent::class.java) return null
        return type.declaredMethods.find {
            it.name == "getHandlerList"
        } ?: findHandler(type.superclass)
    }
    init {
        val handler = findHandler(type.java)?.invoke(null) as HandlerList
        handler.register(RegisteredListener(this, this, NORMAL, STUB_PLUGIN, false))
    }
    override fun execute(listener: Listener, event: BukkitEvent) {
        if (type.java.isAssignableFrom(event.javaClass))
            forEach { it(event) }
    }
}

fun <Type : BukkitEvent> listen(type: KClass<Type>) =
    LISTENER_LISTS.computeIfAbsent(type, ::ListenerList) as Event<(Type).() -> (Unit)>

inline fun <reified Type : BukkitEvent> Toggled.listen(
    noinline listener: (Type).() -> (Unit)
) = listen(Type::class)(listener)
inline fun <reified Type : BukkitEvent> Toggled.cancel(
    noinline listener: (Type).() -> (Boolean)
) where Type : Cancellable = listen<Type> {
    isCancelled = isCancelled || listener()
}


//we can use these kinds of things... or simple create wrappers (probably better)
inline val BlockEvent.world get() = block.world
inline val EntityEvent.world get() = entity.world
inline val PlayerEvent.world get() = player.world

inline val BlockEvent.location get() = block.location
inline val EntityEvent.location get() = entity.location
inline val PlayerEvent.location get() = player.location


val Player.onRightClick get(): Event<PlayerInteractEvent.() -> (Unit)> = {
    listen<PlayerInteractEvent> {
        if (player === this@onRightClick && action == Action.RIGHT_CLICK_AIR || action == Action.RIGHT_CLICK_BLOCK) it()
    }
}
val Player.onBreakBlock: Event<BlockBreakEvent.() -> (Unit)> get() = {
    listen<BlockBreakEvent> { if (player === this@onBreakBlock) it() }
}
val Player.onHungerChange: Event<FoodLevelChangeEvent.() -> (Unit)> get() = {
    listen<FoodLevelChangeEvent> { if (player === this@onHungerChange) it() }
}
val Player.onDamage: Event<EntityDamageEvent.() -> (Unit)> get() = {
    listen<EntityDamageEvent> { if (entity === this@onDamage) it() }
}
val Player.onAdvance: Event<(Advancement) -> (Unit)> get() = {
    listen<PlayerAdvancementDoneEvent> { if (player === this@onAdvance) it(advancement) }
}
val Entity.onDeath: Event<EntityDeathEvent.() -> (Unit)> get() = {
    listen<EntityDeathEvent> { if (entity === this@onDeath) it() }
    listen<PlayerDeathEvent> { if (entity === this@onDeath) it() }
}