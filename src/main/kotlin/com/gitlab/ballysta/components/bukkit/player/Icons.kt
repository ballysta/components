package com.gitlab.ballysta.components.bukkit.player

import com.gitlab.ballysta.architecture.PublishedObservable
import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.components.handle
import net.minecraft.server.v1_16_R3.DataWatcherSerializer
import net.minecraft.server.v1_16_R3.PacketPlayOutEntityEquipment
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.inventory.ItemStack
import java.util.*


val Player.displayHeadIcon: Toggled.(location: Location, isSmall: Boolean, item: PublishedObservable<ItemStack>, x: PublishedObservable<Double>, y: PublishedObservable<Double>, z: PublishedObservable<Double>) -> (Unit)
    get() = { location: Location, isSmall: Boolean, item: PublishedObservable<ItemStack>, x: PublishedObservable<Double>, y: PublishedObservable<Double>, z: PublishedObservable<Double> ->
        var iconId: Int? = null;
        fun sendUpdateEntity() {
            handle(0x44) {

                writeVarInt(iconId!!)

                writeByte(0)
                writeVarInt(0)
                writeByte(0x20)

                writeByte(3)
                writeVarInt(7)
                writeBoolean(false)

                writeByte(5)
                writeVarInt(7)
                writeBoolean(true)

                if (isSmall) {
                    writeByte(15)
                    writeVarInt(0)
                    writeByte(0x01)
                }

                writeByte(15)
                writeVarInt(0)
                writeByte(0x10)

                writeByte(16)
                writeVarInt(8)
                writeFloat(((x.last * Math.PI) / 180).toFloat())
                writeFloat(((y.last * Math.PI) / 180).toFloat())
                writeFloat(((z.last * Math.PI) / 180).toFloat())

                writeByte(255)

            }
        }

        fun sendEntityEquipment() {
            handle(0x47) {
                writeVarInt(iconId!!)

            }
        }

        fun sendSpawnEntity(): Int {
            idCounter--;
            handle(0x02) {
                writeVarInt(idCounter)
                writeUUID(UUID.randomUUID())
                writeVarInt(1)
                writeDouble(location.x)
                writeDouble(location.y)
                writeDouble(location.z)
                writeByte((0 * 256.0f / 360.0f).toInt())
                writeByte((0 * 256.0f / 360.0f).toInt())
                writeByte((0 * 256.0f / 360.0f).toInt())
                writeShort(0)
                writeShort(0)
                writeShort(0)
            }
            sendUpdateEntity()
            return idCounter
        }

        fun sendRemoveEntity() {
            handle(0x36) {
                writeVarInt(1)
                writeVarInt(iconId!!)
            }
        }
        x {
            sendUpdateEntity()
        }
        y {
            sendUpdateEntity()
        }
        z {
            sendUpdateEntity()
        }
        onEnabled {
            iconId = sendSpawnEntity()
        }
        onDisabled {
            sendRemoveEntity()
        }
    }
