package com.gitlab.ballysta.components.bukkit.player

import com.gitlab.ballysta.architecture.*
import com.gitlab.ballysta.architecture.Observable
import com.gitlab.ballysta.components.bukkit.every
import com.gitlab.ballysta.components.bukkit.listen
import com.gitlab.ballysta.components.bukkit.tick
import org.bukkit.Bukkit
import org.bukkit.Bukkit.getOnlinePlayers
import org.bukkit.Location
import org.bukkit.Sound
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.event.player.PlayerJoinEvent
import org.bukkit.event.player.PlayerQuitEvent
import org.bukkit.util.Vector
import java.util.*

object online : Mutated<UUID, Player> {
    override fun get(key: UUID) = Bukkit.getPlayer(key)
    override val onChanged: ChangeEvent<UUID, Player> = { listener ->
        listen<PlayerJoinEvent> { listener(player.id, null, player) }
        listen<PlayerQuitEvent> { listener(player.id, player, null) }
    }

    override val keys get() = values.map { it.uniqueId }
    override val values get() = getOnlinePlayers().enumerator()
}

val UUID.player get() = Bukkit.getPlayer(this)
val UUID.offline get() = Bukkit.getOfflinePlayer(this)
val Player.id get() = uniqueId

fun CommandSender.send(message: Any?) = sendMessage(message.toString())
fun broadcast(message: Any?)
    = online.broadcast(message)
fun broadcast(sound: Sound, volume: Float = 1f, pitch: Float = 1f)
    = online.broadcast(sound, volume, pitch)

fun Mutated<*, Player>.broadcast(message: Any?)
    = forEach { _, player -> player.send(message) }
fun Mutated<*, Player>.broadcast(sound: Sound, volume: Float = 1f, pitch: Float = 1f)
    = forEach { _, player -> player.playSound(player.location, sound, volume, pitch) }

fun Player.teleport(vector: Vector) = teleport(vector.toLocation(world))
val Player.position get(): Observable<Location> = { observer ->
    var last = location
    every(tick)() {
        if (last != location) {
            last = location
            observer(last)
        }; true
    }
}

val Player.blockX get() = location.blockX
val Player.blockY get() = location.blockY
val Player.blockZ get() = location.blockZ
val Player.x get() = location.x
val Player.y get() = location.y
val Player.z get() = location.z