package com.gitlab.ballysta.components.bukkit.player

import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.components.bukkit.every
import com.gitlab.ballysta.components.bukkit.tick
import org.bukkit.Location
import org.bukkit.entity.Player
import org.bukkit.util.Vector

val Player.freeze get(): Toggled.() -> (Unit) = {
    var position: Location? = null
    var speed = -1f
    var flying = false
    var allowed = false

    onEnabled {
        position = location
        speed = flySpeed
        flying = isFlying
        allowed = allowFlight

        allowFlight = true
        flySpeed = 0f
        isFlying = true
        velocity = Vector(0, 0, 0)
    }
    every(tick) {
        if (position!!.distanceSquared(location) > 0.2)
            teleport(position!!)
        true
    }
    onDisabled {
        if (position != null) {
            flySpeed = speed
            isFlying = flying
            allowFlight = allowed
            position = null
        }
    }
}